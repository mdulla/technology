/**
 * Represents Introduction Section by using class component
 */
import React from "react";

class Introduction extends React.Component {
  render() {
    return (
      <>
        <section id="intro">
          <div id="intro-info">
            <div>
              <h1>Full Service Mobile App Development Company</h1>
              <div id="intro-tag-btn">
                <span>Over 100M app downloads across 1500+ projects.</span>
                <a href="" className="brand-btn">
                  Let's Talk
                </a>
              </div>
            </div>
          </div>

          <div id="development-img">
            <img
              src="https://www.dropbox.com/s/7hwnjccu15vt90e/mobileDevlopment.svg?raw=1"
              alt="Mobile App Development"
              title="Mobile App Development"
            />
          </div>
        </section>
      </>
    );
  }
}

export default Introduction;
