import React from "react";

import Footer from "./components/common/Footer";
import Header from "./components/common/header";
import Navigation from "./components/common/Navi";
import Companies from "./components/dependencies/companies";
import Delivary from "./components/dependencies/Delivary";
import Highlights from "./components/dependencies/Highlights";
import Introduction from "./components/dependencies/Intro";
import Partners from "./components/dependencies/Partners";
import Services from "./components/dependencies/Services";
import Story from "./components/dependencies/Success";
import Toplist from "./components/dependencies/Toplist";
import "./App.css";
/**
 * Represents the Main Class Component
 */

class App extends React.Component {
  /**
   * Object for companies Component
   * Passing src,alt,title object properties
   */

  logos = [
    {
      src: "https://www.dropbox.com/s/lmvtthec9yn0ti6/Allianz.png?raw=1",
      alt: "Allianz",
      title: "Work with Allianz",
    },
    {
      src: "https://www.dropbox.com/s/kotgq2u4qr34i2u/audi.jpg?raw=1",
      alt: "Audi",
      title: "Work with Audi",
    },
    {
      src: "https://www.dropbox.com/s/t5dapt3lkz7rdhe/BMW.png?raw=1",
      alt: "BMW",
      title: "Work with BMW",
    },
    {
      src: "https://www.dropbox.com/s/ocqbsbgj590ztyy/ESPN.png?raw=1",
      alt: "ESPN",
      title: "Work with ESPN",
    },
    {
      src: "https://www.dropbox.com/s/2maaqxijcmbaqxg/LG.png?raw=1",
      alt: "LG",
      title: "Work with LG",
    },
    {
      src: "https://www.dropbox.com/s/yn3gj203hrdjfu7/Logo_NIKE.png?raw=1",
      alt: "Nike",
      title: "Work with Nike",
    },
    {
      src: "https://www.dropbox.com/s/gfxa6exv7h1ro6q/Suzuki_logo.png?raw=1",
      alt: "Suzuki",
      title: "Work with Suzuki",
    },
    {
      src: "https://www.dropbox.com/s/b7vwmjf6e0owybv/Visa.svg?raw=1",
      alt: "Visa",
      title: "Work with Visa",
    },
  ];
  /**
   * Object for Partners Component
   * Passing src,alt,title object properties
   */

  logosp = [
    {
      src: "https://www.dropbox.com/s/mk5ca04seizpf8l/aws.svg?raw=1",
      alt: "Work with AWS",
      title: "Our Work",
    },
    {
      src: "https://www.dropbox.com/s/r9utt5nj9k9m1t8/Dell.png?raw=1",
      alt: "Dell",
      title: "Work with Dell",
    },
    {
      src: "https://www.dropbox.com/s/umw9g0zgm1ecfvn/Intel.png?raw=1",
      alt: "intel",
      title: "Work with intell",
    },
    {
      src: "https://www.dropbox.com/s/x0hrha2dosey99z/ibm.png?raw=1",
      alt: "IBM",
      title: "Work with IBM",
    },
    {
      src: "https://www.dropbox.com/s/ekzu1wcki6jziay/Microsoft.svg?raw=1",
      alt: "Microsoft",
      title: "WWork with Microsoft",
    },
    {
      src: "https://www.dropbox.com/s/lvl5cp14i3v0wgi/Nasscom.png?raw=1",
      alt: "Nasscom",
      title: "Work with Nasscom",
    },
    {
      src: "https://www.dropbox.com/s/h66k9jaaknxaum4/Samsung.png?raw=1",
      alt: "Samsung",
      title: "Work with Samsung",
    },
    {
      src: "https://www.dropbox.com/s/86cbtf78khj0q9z/Nvidia.png?raw=1",
      alt: "Nvidia",
      title: "Work with Nvidia",
    },
  ];
  /**
   * Object for Toplist Component
   * Passing src link by using props
   */

  images = [
    { src: "https://www.dropbox.com/s/19czj59oq0orbfa/tm.png?raw=1" },
    { src: "https://www.dropbox.com/s/130734rofy1f261/tata.png?raw=1" },
    {
      src: "https://www.dropbox.com/s/k17kwv9hiu9w98d/Infosys_logo.png?raw=1",
    },
    { src: "https://www.dropbox.com/s/mm4cnforc4pvwac/Wipro_Logo.png?raw=1" },
    {
      src: "https://www.dropbox.com/s/n4scpig8b3tfqkq/Amazon_logo.svg?raw=1",
    },
  ];
  /**
   * Object for Service Component
   * Passing class,title object properties
   */

  icons = [
    { class: "fas fa-laptop", title: "Stratagy and Consultant" },
    { class: "fas fa-users", title: "User Experience Design" },
    { class: "fas fa-mobile-alt", title: "Mobile App Development" },
    { class: "fab fa-chrome", title: "Web App Development" },
    { class: "fas fa-ribbon", title: "Quality Analysis and Testing" },
    { class: "fas fa-ticket-alt", title: "Application Management & Support" },
  ];

  render() {
    return (
      <>
        <Header />
        <Navigation />
        <Introduction />
        <Delivary />
        <Services logos_service={this.icons} />
        <Story />
        <Companies logo_shortcuts={this.logos} />
        <Highlights />
        <Partners logo_partners={this.logosp} />
        <Toplist logo_toplist={this.images} />
        <Footer />
      </>
    );
  }
}
//will be exposed globally
export default App;
