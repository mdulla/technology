import React from "react";

class Companies extends React.Component {
  render() {
    return (
      <div>
        <section id="revenue" className="brand-logos">
          <h1 className="sec-heading">
            We Drive Growth & Revenue for the Best Companies
          </h1>
          <div>
            {this.props.logo_shortcuts.map((data) => {
              return (
                <a key={data.src}>
                  <img src={data.src} alt={data.alt} title={data.title} />
                </a>
              );
            })}
          </div>
        </section>
      </div>
    );
  }
}

export default Companies;
