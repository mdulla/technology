/**
 * Represents Services Section by using class component
 */
import React from "react";

class Services extends React.Component {
  render() {
    return (
      <div>
        <section id="services">
          <h1 className="sec-heading">Our Services</h1>
          <ul>
            {this.props.logos_service.map((data) => {
              return (
                <li key={data.title}>
                  <div>
                    <a href="/">
                      <i className={data.class}></i>
                      <span>{data.title}</span>
                    </a>
                  </div>
                </li>
              );
            })}
          </ul>

          <div id="service-footer">
            <a href="/" className="brand-btn">
              View All Service
            </a>
          </div>
        </section>
      </div>
    );
  }
}

export default Services;
