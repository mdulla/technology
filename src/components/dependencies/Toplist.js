/**
 * Represents Toplist Section by using class component
 */
import React from "react";

class Toplist extends React.Component {
  render() {
    return (
      <div>
        <section id="topList" className="brand-logos">
          <h1 className="sec-heading">
            Recognition as Top Mobile Development Company
          </h1>
          <div>
            {this.props.logo_toplist.map((data) => {
              return (
                <a  key={data.src}>
                  <img
                    src={data.src}
                   
                    alt="Top 10 MobleApp Development Companies"
                    title="Top 10 MobleApp Development Companies"
                  />
                  <span>
                    Recognised Among Top 10 MobleApp Development Companies
                  </span>
                </a>
              );
            })}
          </div>
        </section>
      </div>
    );
  }
}

export default Toplist;
