/**
 * Represents partners Section by using class component
 */
import React from "react";

  
  class Partners extends React.Component{
    
    render(){
    return (
      <div>
        <section id="partners" className="brand-logos">
          <h1 className="sec-heading">Our Partners</h1>
          <div>
            {this.props.logo_partners.map((data) => {
              return (
                <a key={data.src} >
                  <img src={data.src} alt={data.alt} title={data.title} />
                </a>
              );
            })}
          </div>
        </section>
      </div>
    );
  }};
  
  export default Partners;
  
  
