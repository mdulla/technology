function Services(){
  return(
    <>
    <section id="services">
        <h1 className="sec-heading">Our Services</h1>
        <ul>
            <li>
                <div>
                    <a href="">
                        <i className="fas fa-laptop"></i><span>Stratagy and Consultant</span>
                    </a>
                </div>
            </li>
            <li>
                <div>
                    <a href="">
                        <i className="fas fa-users"></i><span>User Experience Design</span>
                    </a>
                </div>
            </li>
            <li>
                <div>
                    <a href="">
                        <i className="fas fa-mobile-alt"></i><span>Mobile App Development</span>
                    </a>
                </div>
            </li>
            <li>
                <div>
                    <a href="">
                        <i className="fab fa-chrome"></i><span>Web App Development</span>
                    </a>
                </div>
            </li>
            <li>
                <div>
                    <a href="">
                        <i className="fas fa-ribbon"></i><span>Quality Analysis and Testing</span>
                    </a>
                </div>
            </li>
            <li>
                <div>
                    <a href="">
                        <i className="fas fa-ticket-alt"></i><span>Application Management & Support</span>
                    </a>
                </div>
            </li>
        </ul>
        
        <div id="service-footer">
            <a href="" className="brand-btn">View All Service</a>
        </div>
    </section>
   </>
   )
  }
      
   export  default Services  