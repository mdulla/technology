/**
 * Represents the Navbar Component
 */

import React from "react";

class Navigation extends React.Component {
  
  render() {
    return (
      <>
        <nav>
          <span className="logo">iTechnology</span>
          <div className="menu-btn-3" onClick={()=>{}}>
            <span></span>
          </div>
          <div className="mainMenu">
            <a href="">
              <span>Technology</span>
            </a>
            <a href="">
              <span>Service</span>
            </a>
            <a href="">
              <span>Portfolio</span>
            </a>
            <a href="">
              <span>About Us</span>
            </a>
            <a href="">
              <span>Career</span>
            </a>
            <a href="">
              <span>Blog</span>
            </a>
            <a href="">Work With Us</a>
          </div>
        </nav>
      </>
    );
  }
}

export default Navigation;
