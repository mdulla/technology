/**
 * Represents the Header Component
 */

import React from "react";

class Header extends React.Component {
  render() {
    return (
      <>
        <header id="topHeader">
          <ul id="topInfo">
            <li>+974 98765432</li>
            <li>info@itecnology.com</li>
          </ul>
        </header>
      </>
    );
  }
}
export default Header;
