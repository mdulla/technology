import React from "react";

/**
 * Represents Delivary Section by using class component
 */

class Delivary extends React.Component {
  render() {
    return (
      <>
        <section id="delivery">
          <h1 className="sec-heading">Delivering Experience Since 2009</h1>
          <div className="col-5 delivery-img">
            <img
              src="https://www.dropbox.com/s/ipx91osglyczpdt/delivery_experience.svg?raw=1"
              alt="Productivity Delivering Experience"
              title="Delivering Experience Since 2009"
            />
          </div>
          <div className="col-7">
            <h2>Accelerating your business growth with Digital Experiences</h2>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
              <br />
              <br /> It has survived not only five centuries, but also the leap
              into electronic typesetting, remaining essentially unchanged. It
              was popularised in the 1960s with the release of Letraset sheets
              containing Lorem Ipsum passages, and more recently with desktop
              publishing software like Aldus PageMaker including versions of
              Lorem Ipsum.
            </p>
            <div className="btn-footer">
              <a href="" className="brand-btn">
                Contact Us
              </a>
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default Delivary;
